# sisop-praktikum-modul-1-2023-BJ-A09

Perkenalkan kami dari kelas sistem operasi kelompok A09, dengan anggota sebagai berikut:


|Nama                   |NRP        |
|---|---|
|Muhammad Rifqi Fadhilah|5025211228 |
|Muhammad Naufal Baihaqi|5025211103 |
|Tigo S Yoga            |5025211125 |

berikut ini adalah penjelasan dari praktikum modul 1 sisop A 

# Nomor 1
Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  :

### Nomor 1 A

Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang

Jawaban : 

``` 
awk -F',' '$4=="Japan" {print}' QSW.xls | head -5
``` 

Penjelasan : 

1. `awk` adalah perintah untuk memanipulasi data dalam file pada baris perintah.
2. `-F','` mengatur pemisah antar kolom menjadi tanda koma. Artinya `awk` akan memisahkan data masukan menjadi kolom-kolom berdasarkan tanda koma.
3. `'$4=="Japan" {print}'` adalah pola dan tindakan yang akan dieksekusi oleh `awk`. Polanya adalah `$4=="Japan"`, yang artinya `awk` hanya akan menjalankan tindakan pada baris-baris di mana kolom keempat sama dengan "Japan". Tindakan yang diambil adalah {print}, yang artinya awk akan mencetak seluruh kolom yang memenuhi pola tersebut.
4. `QSW.xls` adalah file masukan yang akan dibaca oleh `awk`.
5. `| head -5` mengalirkan keluaran dari `awk` ke perintah head, yang mencetak 5 baris pertama dari keluaran.

### Nomor 1 B

Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang. 

Jawaban :

```
awk -F',' '$4=="Japan" {print $9","$2","$4","$1}' QSW.xls | head -5 | sort -n | tail -5
```

Penjelasan :

1. `awk` adalah perintah untuk memanipulasi data dalam file pada baris perintah.
2. `-F','` mengatur pemisah antar kolom menjadi tanda koma. Artinya `awk` akan memisahkan data masukan menjadi kolom-kolom berdasarkan tanda koma.
3. `'$4=="Japan" {print $9","$2","$4","$1}'` adalah pola dan tindakan yang akan dieksekusi oleh `awk`. Polanya adalah `$4=="Japan"`, yang artinya awk hanya akan menjalankan tindakan pada baris-baris di mana kolom keempat sama dengan "Japan". Tindakan yang diambil adalah `{print $9","$2","$4","$1}`, yang artinya awk akan mencetak kolom ke-9, ke-2, ke-4, dan ke-1 dari baris yang memenuhi pola tersebut, dipisahkan dengan tanda koma.
4. `QSW.xls` adalah file masukan yang akan dibaca oleh `awk`.
5. `| head -5` mengalirkan keluaran dari `awk` ke perintah `head`, yang mencetak 5 baris pertama dari keluaran.
6. `| sort -n` mengalirkan keluaran dari `head` ke perintah `sort`, yang akan mengurutkan keluaran secara numerik berdasarkan nilai pada kolom pertama (kolom ke-9 dari file asli, karena kolom-kolom dipisahkan dengan tanda koma).
7. `| tail -5` mengalirkan keluaran dari `sort` ke perintah `tail`, yang mencetak 5 baris terakhir dari keluaran, yang merupakan 5 baris dengan nilai pada kolom pertama yang terbesar.

### Nomor 1 C

Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.

Jawaban :

```
awk -F',' '$4=="Japan" {print $18","$2","$4","$1}' QSW.xls | sort -n | head -10
```

Penjelasan :

1. `awk` adalah perintah untuk memanipulasi data dalam file pada baris perintah.
2. `-F','` mengatur pemisah antar kolom menjadi tanda koma. Artinya `awk` akan memisahkan data masukan menjadi kolom-kolom berdasarkan tanda koma.
3. `'$4=="Japan" {print $18","$2","$4","$1}'` adalah pola dan tindakan yang akan dieksekusi oleh `awk`. Polanya adalah `$4=="Japan"`, yang artinya awk hanya akan menjalankan tindakan pada baris-baris di mana kolom keempat sama dengan "Japan". Tindakan yang diambil adalah `{print $18","$2","$4","$1}`, yang artinya awk akan mencetak kolom ke-18, ke-2, ke-4, dan ke-1 dari baris yang memenuhi pola tersebut, dipisahkan dengan tanda koma.
4. `QSW.xls` adalah file masukan yang akan dibaca oleh `awk`.
5. `| sort -n` mengalirkan keluaran dari `awk` ke perintah `sort`, yang akan mengurutkan keluaran secara numerik berdasarkan nilai pada kolom pertama (kolom ke-18 dari file asli, karena kolom-kolom dipisahkan dengan tanda koma).
6. `| head -10` mengalirkan keluaran dari `sort` ke perintah `head`, yang mencetak 10 baris pertama dari keluaran, yang merupakan 10 baris dengan nilai pada kolom pertama yang terkecil.

### Nomor 1 D 

Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

Jawaban :

```
awk '/Keren/{print}' QSW.xls
```

Penjelasan :

1. `awk` adalah perintah untuk memanipulasi data dalam file pada baris perintah.
2. `'/Keren/{print}'` adalah pola dan tindakan yang akan dieksekusi oleh `awk`. Polanya adalah `/Keren/`, yang artinya `awk` hanya akan menjalankan tindakan pada baris-baris yang mengandung teks "Keren". Tindakan yang diambil adalah `{print}`, yang artinya awk akan mencetak baris yang memenuhi pola tersebut.
3. `QSW.xls` adalah file masukan yang akan dibaca oleh `awk`

Hasil program
![soal1](https://user-images.githubusercontent.com/88433109/224337089-5555556a-e168-4fd3-979a-bc292c145989.JPG)


# Nomor 2

Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.

### Nomor 2 A

Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:

* File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
* File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) 

Jawaban :

```
i=1

while [ -d kumpulan_$i ]
do
    i=$((i+1))
done

mkdir kumpulan_$i
cd kumpulan_$i

for ((j=1; j<=$x; j++))
do
    wget "https://loremflickr.com/320/240/indonesia,bali" -O perjalanan_$j.jpg
done

cd ..
```

Penjelasan :

1. `i=1:` inisialisasi variabel i dengan nilai 1.
2. `while [ -d kumpulan_$i ]`: melakukan loop while selama masih ada direktori dengan nama "kumpulan_$i".
3. `do`: awal dari loop.
4. `i=$((i+1))`: menambahkan nilai variabel i dengan 1.
5. `done`: akhir dari loop.
6. `mkdir kumpulan_$i`: membuat direktori baru dengan nama "kumpulan_$i".
7. `cd kumpulan_$i`: pindah ke direktori yang baru saja dibuat.
8. `for ((j=1; j<=$x; j++))`: melakukan loop for sebanyak x kali, dimana x adalah waktu saat ini dalam format jam.
9. `do`: awal dari loop.
10. `wget "https://loremflickr.com/320/240/indonesia,bali" -O perjalanan_$j.jpg`: mengunduh gambar dari situs LoremFlickr dengan ukuran 320x240 pixel dan tag "indonesia,bali" dengan nama "perjalanan_$j.jpg".
11. `done`: akhir dari loop.
12. `cd ..`: kembali ke direktori sebelumnya.

### Nomer 2 B 

Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

Jawaban :

```
#!bin/bash

x=$(date "+%H")
# x=8  #untuk coba2

# agar waktu saat pertama kali menggunakan program ini bisa tercatat
# hal tersebut dipakai untuk menjadwalkan proses zip yang dilakukan setelah 24 jam dari saat zip sebelumnya

TEMP_FILE="x_awal.txt"

if [ ! -f $TEMP_FILE ]; then
    cd /home/naufalqii
    touch x_awal.txt
    cd Documents/prak_sisop/soal2
    touch x_awal.txt
    echo "$x" > x_awal.txt
fi

j=1
while [ -f devil_$j.zip ]
do
    j=$((j+1))
done

# membuat zip
i=1
if [ $(cat x_awal.txt) -eq $x ]; then
    while [ -d kumpulan_$i ]
    do
        zip -r devil_$j.zip *kumpulan*
            i=$((i+1))
    done
    rm -r *kumpulan*
fi


# crontab -e
# 0 */10 * * * /bin/bash /home/naufalqii/Documents/prak_sisop/soal2/kobeni_liburan.sh
```

Penjelasan :

1. `#!bin/bash` mengatur shebang untuk mengindikasikan bahwa script ini akan dijalankan oleh bash.
2. `x=$(date "+%H")` mendefinisikan variabel x dengan nilai jam saat ini menggunakan command 'date'.
3. `TEMP_FILE="x_awal.txt"
    if [ ! -f $TEMP_FILE ]; then
    cd /home/naufalqii
    touch x_awal.txt
    cd Documents/prak_sisop/soal2
    touch x_awal.txt
    echo "$x" > x_awal.txt
    fi`
merupakan blok kode yang digunakan untuk membuat file `"x_awal.txt"` pada direktori yang ditentukan jika file tersebut belum ada. File ini digunakan untuk menyimpan jam terakhir kali script dijalankan, sehingga bisa dijadwalkan untuk membuat file zip setelah 24 jam dari waktu terakhir kali script dijalankan.
4. `while [ -d kumpulan_$i ] 
    do
    i=$((i+1))
done`
adalah loop while yang digunakan untuk menentukan nomor indeks i dari direktori yang akan dibuat. Loop akan terus berjalan hingga tidak ada direktori dengan nama "kumpulan_i" yang ada di direktori saat ini. Variabel i bertambah 1 setiap kali loop berjalan.
5. `mkdir kumpulan_$i` membuat direktori baru dengan nama "kumpulan_i".
6. `for ((j=1; j<=$x; j++))
do wget "https://loremflickr.com/320/240/indonesia,bali" -O perjalanan_$j.jpg
done`
adalah loop for yang akan mengunduh gambar dari situs "loremflickr.com" sebanyak x kali dan menyimpannya dalam direktori "kumpulan_i" dengan nama "perjalanan_j.jpg", dimana j adalah nomor indeks gambar.
7. `cd ..` mengembalikan program ke direktori sebelumnya.
8. `j=1
while [ -f devil_$j.zip ]
do
    j=$((j+1))
done`
adalah loop while yang digunakan untuk menentukan nomor indeks j dari file zip yang akan dibuat. Loop akan terus berjalan hingga tidak ada file dengan nama "devil_j.zip" yang ada di direktori saat ini. Variabel j bertambah 1 setiap kali loop berjalan.
9. `i=1
if [ $(cat x_awal.txt) -eq $x ]; then
    while [ -d kumpulan_$i ]
    do
        zip -r devil_$j.zip *kumpulan*
            i=$((i+1))
    done
    rm -r *kumpulan*
fi`
merupakan blok kode yang akan membuat file zip jika waktu saat ini sama dengan waktu terakhir kali script dijalankan. Loop while akan menentukan nomor indeks i dari direktori yang akan di-zip. Loop akan terus berjalan hingga tidak ada direktori dengan nama "kumpulan_i" yang ada di direktori saat ini. Variabel i bertambah 1 setiap kali loop berjalan. Setelah itu, perintah 'zip' akan membuat file zip dengan nama "devil_j.zip" dari semua file yang ada di direktori "kumpulan_i". Selain itu, perintah 'rm' akan menghapus semua direktori yang di-zip.
10. `# crontab -e # 0 */10 * * * /bin/bash /home/naufalqii/Documents/prak_sisop/soal2/kobeni_libur`
 digunakan untuk mengatur jadwal menjalankan script ini menggunakan crontab. Script ini akan dijalankan setiap 10 jam sekali.

Hasil Program
![no2](https://user-images.githubusercontent.com/88433109/224348041-7cd31d48-fbec-4aaf-9437-8109e7888b20.jpg)
![no2zip](https://user-images.githubusercontent.com/88433109/224348053-25704738-71bf-4783-b47f-4ec677fab876.jpg)

# Nomor 3

Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh

### Nomor 3 A 

Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut:
* Minimal 8 karakter
* Memiliki minimal 1 huruf kapital dan 1 huruf kecil
* Alphanumeric
* Tidak boleh sama dengan username 
* Tidak boleh menggunakan kata chicken atau ernie

Jawaban:

* pada file louis.sh jawaban 3 A terdapat pada syntax:
```
#!/bin/bash

validate_reg_username() {
    if grep -qi "u-$username" users/users.txt; then
        echo "Username sudah terdaftar, silahkan gunakan username lain !"
else
        return 0
    fi

    return 1
}

validate_reg_password() {
    if [[ ${#password} -lt 8 ]]; then
        echo "Password minimal 8 karakter !"
        return 1
    fi

    if ! [[ $password =~ [[:upper:]] && $password =~ [[:lower:]] ]]; then
        echo "Password harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
        return 1
    fi

    if ! [[ $password =~ ^[a-zA-Z0-9]+$ ]]; then
        echo "Password harus alphanumeric"
        return 1
    fi

    if ! [[ $password =~ [0-9] ]]; then
        echo "Password harus mengandung angka"
        return 1
    fi

    if [[ $password == $username ]]; then 
        echo "Password tidak boleh sama dengan username"
        return 1
    fi

    if echo ${password} | grep -i 'chicken\|ernie'; then 
        echo "Password tidak boleh mengandung kata chicken atau ernie"
        return 1
    fi

}

if [ -e users ]; then
    cd users
    touch users.txt
    cd ..
else
    mkdir users
    cd users
    touch users.txt
    cd ..
fi

timestamp="$(date '+%y/%m/%d %H:%M:%S')"

echo -e "\n------------------REGISTER------------------"

while true; do
read -p "Masukkan username: " username

if validate_reg_username; then
    break;
else
    continue;
fi
done

while true; do
read -p "Masukkan password: " password

if validate_reg_password; then
    break;
else
    continue;
fi
done

echo -e "\n"


echo "u-$username:p-$password" >> users/users.txt

echo "Registrasi berhasil !"
```

* pada file retep.sh jawaban 3 A terdapat pada syntax:

```
#!/bin/bash

timestamp="$(date '+%y/%m/%d %H:%M:%S')"

validate_log_username(){
    if ! grep -q "^u-$username" users/users.txt; then
        echo "Username tidak ada !"
        return 1;
    fi
        return 0;
}


validate_log_password(){
    if grep -q "^u-$username:p-$password$" users/users.txt; then
        echo "Login berhasil"
        return 0
    else
        echo "Password salah!"
    fi
        return 1
}

echo -e "\n------------------LOGIN------------------"

while true; do
read -p "Masukkan username: " username

if validate_log_username; then 
    break;
else
    continue;
fi
done


while true; do
read -p "Masukkan password: " password

if validate_log_password; then
    break;
else
    continue;
fi
done

```

Penjelasan:

* berikut ini adalah penjelasan pada file louis.sh
    * `#!/bin/bash`: Ini adalah shebang, yaitu karakter #! diikuti dengan lokasi program shell yang akan digunakan untuk menjalankan skrip Bash ini.
    * fungsi `validate_reg_username`digunakan untuk memvalidasi apakah username yang dimasukkan sudah terdaftar atau belum pada file `users.txt`. Pertama-tama, fungsi menggunakan perintah `grep` untuk mencari apakah terdapat baris pada file `users.txt` yang mengandung string `"u-$username"` dengan opsi `-q` untuk menjalankan perintah secara "silent" atau tanpa menampilkan output pada layar. Jika baris yang mengandung string tersebut ditemukan, maka fungsi akan mencetak pesan kesalahan pada layar dan mengembalikan nilai `1` untuk menandakan bahwa validasi gagal. Jika tidak ada baris yang mengandung string tersebut, maka fungsi akan mengembalikan nilai `0` untuk menandakan bahwa validasi berhasil.

    * Fungsi `validate_reg_password` digunakan untuk memvalidasi apakah password yang dimasukkan oleh pengguna sudah memenuhi kriteria yang ditentukan. Pertama-tama, fungsi melakukan pengecekan terhadap panjang password dengan menggunakan operator `${#password}` yang akan mengembalikan panjang string pada variabel `password`. Jika panjang password kurang dari 8 karakter, fungsi akan mencetak pesan kesalahan pada layar dan mengembalikan nilai `1` untuk menandakan bahwa validasi gagal. Selanjutnya, fungsi melakukan pengecekan apakah password sudah memenuhi kriteria minimal 1 huruf kapital dan 1 huruf kecil dengan menggunakan operator `[[ ]]` yang mendukung penggunaan regex pada bash. Jika password belum memenuhi kriteria tersebut, fungsi akan mencetak pesan kesalahan pada layar dan mengembalikan nilai `1` untuk menandakan bahwa validasi gagal. Selanjutnya, fungsi melakukan pengecekan apakah password hanya mengandung karakter alphanumeric (huruf dan angka) dengan menggunakan regex pada baris ke-20. Jika password mengandung karakter selain alphanumeric, fungsi akan mencetak pesan kesalahan pada layar dan mengembalikan nilai `1` untuk menandakan bahwa validasi gagal. Pada baris ke-30 sampai 33, fungsi melakukan pengecekan apakah password mengandung angka. Jika tidak, fungsi akan mencetak pesan kesalahan pada layar dan mengembalikan nilai 1 untuk menandakan bahwa validasi gagal.Pada baris ke-35, fungsi melakukan pengecekan apakah password sama dengan username yang dimasukkan oleh pengguna. Jika sama, fungsi akan mencetak pesan kesalahan pada layar dan mengembalikan nilai `1` untuk menandakan bahwa validasi gagal. Terakhir, pada baris ke-40 sampai 45, fungsi melakukan pengecekan apakah password mengandung kata "chicken" atau "ernie". Jika ya, fungsi akan mencetak pesan kesalahan pada layar dan mengembalikan nilai `1` untuk menandakan bahwa validasi gagal. Jika semua validasi berhasil, maka fungsi akan mengembalikan nilai `0` untuk menandakan bahwa valid

    * `if [ -e users ];` berfungsi untuk mengecek apakah file atau direktori bernama 'users' ada atau tidak. Jika ada maka lanjut ke script `cd users` untuk masuk ke direktori users, `touch users.txt` untuk membuat file users.txt jika belum ada file tersebut dan jika sudah ada, maka command tersebut digunakan untuk memperbarui waktu modifikasi. `cd ..` untuk pindah ke direktori induk. Jika direktori bernama 'users' belum ada maka lanjut ke script `mkdir users` untuk membuat direktori users, `cd users` untuk masuk ke direktori users, `touch users.txt` untuk membuat file users.txt, `cd ..` untuk pindah ke direktori induk.

    * `timestamp="$(date '+%y/%m/%d %H:%M:%S')"`: Baris ini digunakan untuk mendapatkan waktu saat ini dalam format `YY/MM/DD HH:MM:SS` dan menyimpannya ke dalam variabel `timestamp`.

    * `echo -e "\n------------------REGISTER------------------"`: Baris ini menampilkan tulisan "REGISTER" di dalam bentuk border.

    * `while true; do`: Baris ini memulai loop while yang berjalan terus menerus selama kondisinya benar (true).

    * `read -p "Masukkan username: " username`: Baris ini menampilkan pesan "Masukkan username: " dan menyimpan input dari user ke dalam variabel `username`.

    * `if validate_reg_username; then`: Baris ini memanggil fungsi `validate_reg_username()` dan mengecek apakah username yang diinput sudah terdaftar. Jika username sudah terdaftar, maka akan menampilkan pesan "Username sudah terdaftar, silahkan gunakan username lain !" dan loop akan berlanjut dari awal. Jika username belum terdaftar, maka loop akan berhenti.

    * `break;`: Baris ini digunakan untuk menghentikan loop while.

    * `else continue;`: Baris ini digunakan untuk melanjutkan loop while ke iterasi berikutnya.

    * `done`: Baris ini menutup loop while.

    * `while true; do`: Baris ini memulai loop while yang berjalan terus menerus selama kondisinya benar (true).

    * `read -p "Masukkan password: " password`: Baris ini menampilkan pesan "Masukkan password: " dan menyimpan input dari user ke dalam variabel password.

    * `if validate_reg_password; then`: Baris ini memanggil fungsi `validate_reg_password()` dan mengecek apakah password yang diinput memenuhi persyaratan yang telah ditentukan. Jika password memenuhi persyaratan, maka loop akan berhenti. Jika password tidak memenuhi persyaratan, maka akan menampilkan pesan error dan loop akan berlanjut dari awal.

    * `break;`: Baris ini digunakan untuk menghentikan loop while.

    * `else continue;`: Baris ini digunakan untuk melanjutkan loop while ke iterasi berikutnya.

    * `done`: Baris ini menutup loop while.

    * `echo -e "\n"`: Baris ini digunakan untuk membuat baris baru.

    * `echo "u-$username:p-$password" >> users/users.txt`: Baris ini menambahkan username dan password ke dalam file `users/users.txt` dengan format `u-username:p-password`.

    * `echo "$timestamp REGISTER: INFO User $username registered successfully" >> log.txt`: Baris ini menambahkan informasi registrasi ke dalam file log.txt dengan format `YY/MM/DD HH:MM:SS REGISTER: INFO User username registered successfully`.

    * `echo "Registrasi berhasil !"`: Baris ini menampilkan pesan "Registrasi berhasil !" di layar.

* berikut ini adalah penjelasan pada file retep.sh

    * `#!/bin/bash`: Ini adalah shebang, yaitu karakter #! diikuti dengan lokasi program shell yang akan digunakan untuk menjalankan skrip Bash ini.

    * `timestamp="$(date '+%y/%m/%d %H:%M:%S')"`: Ini adalah variabel `timestamp` yang digunakan untuk menyimpan tanggal dan waktu saat ini dalam format tahun/bulan/hari jam:menit:detik.

    * `validate_log_username()`: Ini adalah fungsi yang mengecek apakah username yang dimasukkan ada pada file "users.txt". Jika tidak ada maka keluarkan pesan "Username tidak ada !" dan return 1, jika ada maka validasi berhasil dan return 0.

    * `validate_log_password()`: Ini adalah fungsi yang digunakan untuk memvalidasi apakah username dan password yang dimasukkan oleh pengguna telah sesuai dengan data yang ada pada file "users.txt".

    * `if grep -q "^u-$username:p-$password$" users/users.txt; then`: Ini adalah perintah untuk mencari baris dalam file "users.txt" yang cocok dengan username dan password yang dimasukkan oleh pengguna. Jika cocok, fungsi akan mengembalikan nilai 0 dan menampilkan pesan "Login berhasil". Jika tidak cocok, fungsi akan mengembalikan nilai 1 dan menampilkan pesan "Password salah!".

    * `echo -e "\n------------------LOGIN------------------"`: Ini adalah perintah untuk menampilkan pesan "LOGIN" yang akan tampil pada awal saat pengguna akan melakukan login ke dalam sistem.

    * `while true; do`: Ini adalah perulangan tak terbatas yang akan dijalankan selama pengguna tidak berhasil melakukan validasi username.

    * `read -p "Masukkan username: " username`: Ini adalah perintah untuk meminta pengguna memasukkan username-nya.

    * `if validate_log_username()`: Ini adalah perintah untuk memanggil fungsi `validate_log_username()` untuk memvalidasi username yang dimasukkan oleh pengguna. Jika validasi berhasil, maka perulangan akan dihentikan dan pengguna akan lanjut memasukkan password. Jika validasi gagal pengguna diminta memasukkan username kembali.

     * `else continue; fi`: Ini adalah perintah untuk melanjutkan perulangan saat validasi password gagal.

    * `while true; do`: Ini adalah perulangan tak terbatas yang akan dijalankan selama pengguna tidak berhasil melakukan validasi password.

    * `read -p "Masukkan password: " password`: Ini adalah perintah untuk meminta pengguna memasukkan password-nya.

    * `if validate_log_password; then`: Ini adalah perintah untuk memanggil fungsi `validate_log_password()` untuk memvalidasi username dan password yang dimasukkan oleh pengguna. Jika validasi berhasil, maka perulangan akan dihentikan dan pengguna akan berhasil masuk ke dalam sistem. Jika validasi gagal, maka pesan "Password salah!" akan ditampilkan dan pengguna diminta memasukkan password kembali.

    * `else continue; fi`: Ini adalah perintah untuk melanjutkan perulangan saat validasi password gagal.

### Nomor 3 B 

Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
* Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
* Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
* Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
* Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

Jawaban:

* pada file louis.sh jawaban 3 B terdapat pada syntax:
```
echo "${timestamp} REGISTER: ERROR User already exists" >>  log.txt
echo "$timestamp REGISTER: INFO User $username registered successfully" >>  log.txt
```

* pada file retep.sh jawaban 3 B terdapat pada syntax:

```
echo "$timestamp LOGIN: USER $username logged in" >> log.txt
echo "$timestamp LOGIN: ERROR Failed login attempt on username $username" >> log.txt
```

Penjelasan:

* berikut ini adalah penjelasan pada file louis.sh

    *   untuk baris pertama berikut ini adalah penjelasannya

    1. `echo`: perintah ini menulis pesan yang diberikan ke output standar (dalam hal ini, pesan tersebut akan diarahkan ke file log).
    2. `"${timestamp} REGISTER: ERROR User already exists"`: Ini adalah pesan yang akan ditulis ke file log. Variabel `${timestamp}` akan diganti dengan tanggal dan waktu saat ini ketika perintah dieksekusi.
    3. `>> log.txt`: Ini mengarahkan keluaran perintah `echo` ke file "log.txt" dan menambahkannya ke akhir file. Jika file tidak ada, maka akan dibuat baru.

    * pada baris kedua berikut ini adalah penjelasannya

    1. `echo`: perintah ini menulis pesan yang diberikan ke output standar (dalam hal ini, pesan tersebut akan diarahkan ke file log).
    2. `"$timestamp REGISTER: INFO User $username registered successfully"`: Ini adalah pesan yang akan ditulis ke file log. Variabel `$timestamp` dan `$username` akan diganti dengan tanggal dan waktu saat ini serta nama pengguna yang berhasil terdaftar.
    3. `>> log.txt`: Ini mengarahkan keluaran perintah `echo` ke file "log.txt" dan menambahkannya ke akhir file. Jika file tidak ada, maka akan dibuat baru.

* berikut ini adalah penjelasan pada file retep.sh
    * pada baris pertama berikut ini adalah penjelasannya
    
    1. `echo`: perintah ini menulis pesan yang diberikan ke output standar (dalam hal ini, pesan tersebut akan diarahkan ke file log).
    2. `"$timestamp LOGIN: USER $username logged in"`: Ini adalah pesan yang akan ditulis ke dalam file log. Variabel `$timestamp` dan `$username` akan diganti dengan tanggal dan waktu saat ini serta nama pengguna yang berhasil login.
    3. `>> log.txt`: Ini mengarahkan keluaran perintah `echo` ke file "log.txt" dan menambahkannya ke akhir file. Jika file tidak ada, maka akan dibuat baru.


    * pada baris kedua berikut ini adalah penjelasannya

    1. `echo`: perintah ini menulis pesan yang diberikan ke output standar (dalam hal ini, pesan tersebut akan diarahkan ke file log).
    2. `"$timestamp LOGIN: ERROR Failed login attempt on username $username"`: Ini adalah pesan yang akan ditulis ke dalam file log. Variabel `$timestamp` dan `$username` akan diganti dengan tanggal dan waktu saat ini serta nama pengguna yang gagal melakukan login.
    3. `>> log.txt`: Ini mengarahkan keluaran perintah `echo` ke file "log.txt" dan menambahkannya ke akhir file. Jika file tidak ada, maka akan dibuat baru.

Hasil Program
![soal2a](https://user-images.githubusercontent.com/88433109/224340014-8def29a3-759f-44f0-b6ab-50dd9f555de7.JPG)

# Nomor 4

Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan :

* Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
* Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
    * Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
    * Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
    * Setelah huruf z akan kembali ke huruf a
* Buat juga script untuk dekripsinya.
* Backup file syslog setiap 2 jam untuk dikumpulkan

Jawaban :

berikut adalah syntax untuk membuat file log_encrypt.sh

```
# #!/bin/bash

H=$(date +'%H')
y=$(date +"%H:%M_%d:%b:%Y")

sudo cat /var/log/syslog > /home/rifqi/Documents/PraktikumSisop/$y.txt

# buat array untuk mendapatkan value per hurufnya
lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)


# str untuk menyimpan string hasil enkripsi
echo -n "$(cat $y.txt | tr 'a-z' "${lowercase[$H]}-z}a-${lowercase[$H-1]}" | tr 'A-Z' "${uppercase[$H]}-Z}A-${uppercase[$H-1]}")" > "$y.txt"

# 0 */2 * * * /bin/bash /home/rifqi/Documents/PraktikumSisop/log_encrypt.sh

```
berikut adalah syntax untuk membuat file log_decrypt.sh

```
#!/bin/bash

H=$(date +'%H')
y=$(date +'%H:%M_%d:%b:%Y')

sudo cat /var/log/syslog > /home/rifqi/Documents/PraktikumSisop/$y.txt


lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)


echo -n "$(cat $y.txt | tr "${lowercase[$H]}-z}a-${lowercase[$H-1]}" 'a-z' | tr "${uppercase[$H]}-Z}A-${uppercase[$H-1]}" 'A-Z' )" > "$y.txt"


# echo -n "$(cat 21:00_03:Mar:2023.txt | tr "${lowercase[$H]}-z}a-${lowercase[$H-1]}" 'a-z' | tr "${uppercase[$H]}-Z}A-${uppercase[$H-1]}" 'A-Z' )" > "$y.txt"

# crontab
# 0 */2 * * * /bin/bash /home/rifqi/Documents/PraktikumSisop/log_decrypt.sh
```

Penjelasan :

berikut adalah penjelesan syntax log_encrypt.sh

* `H=$(date +'%H')`, script mengambil jam saat ini dengan perintah `date +%H` dan menyimpannya dalam variabel `$H`.

* `y=$(date +"%H:%M_%d:%b:%Y")`, script mengambil waktu saat ini dengan format tertentu (jam:menit_tanggal:bulan:tahun) dengan perintah `date +"%H:%M_%d:%b:%Y"` dan menyimpannya dalam variabel `$y`.

* `sudo cat /var/log/syslog > /home/rifqi/Documents/PraktikumSisop/$y.txt`, script melakukan command `sudo cat` untuk membaca isi file `/var/log/syslog` dan mengalirkan keluaran ke file dengan nama `$y.txt` di direktori `/home/rifqi/Documents/PraktikumSisop/`.

* script membuat dua array: `lowercase` dan `uppercase` untuk menyimpan huruf kecil dan huruf besar.

* Pada baris empat belas, variabel `str` dibuat untuk menyimpan string hasil enkripsi.

* isi file `$y.txt` dienkripsi dengan command `tr`, yaitu:

* `tr 'a-z' "${lowercase[$H]}-z}a-${lowercase[$H-1]}"` mengganti semua huruf kecil dalam file dengan huruf yang di-generate dengan menggeser huruf ke kanan sebanyak `$H` kali.
* `tr 'A-Z' "${uppercase[$H]}-Z}A-${uppercase[$H-1]}"` mengganti semua huruf besar dalam file dengan huruf yang di-generate dengan menggeser huruf ke kanan sebanyak `$H` kali.
* `echo -n` untuk menulis hasil enkripsi ke dalam variabel `str`.
* isi dari `$y.txt` yang telah dienkripsi kemudian ditulis ulang ke file dengan nama yang sama (`$y.txt`) dengan perintah `> "$y.txt"`.

* Pada baris terakhir, terdapat konfigurasi cron job untuk menjalankan script ini secara otomatis setiap 2 jam. Konfigurasi cron job ini ditulis pada crontab dengan perintah `0 */2 * * * /bin/bash /home/rifqi/Documents/PraktikumSisop/log_encrypt.sh`.

berikut adalah penjelesan syntax log_decrypt.sh

* `H=$(date +'%H')` : mendapatkan nilai jam saat script dijalankan dan menyimpannya dalam variabel `H`.
* `y=$(date +'%H:%M_%d:%b:%Y')` : mendapatkan timestamp saat script dijalankan dengan format "jam:menit_tanggal:bulan:tahun" dan menyimpannya dalam variabel `y`.
* `sudo cat /var/log/syslog > /home/rifqi/Documents/PraktikumSisop/$y.txt` : meng-copy isi file syslog ke file baru yang berada di direktori /home/rifqi/Documents/PraktikumSisop dengan nama file sesuai dengan timestamp yang telah didapatkan sebelumnya.
* `lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)` dan `uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)` : deklarasi array yang berisi alfabet lowercase dan uppercase.
*` echo -n "$(cat $y.txt | tr "${lowercase[$H]}-z}a-${lowercase[$H-1]}" 'a-z' | tr "${uppercase[$H]}-Z}A-${uppercase[$H-1]}" 'A-Z' )" > "$y.txt"` : mengambil isi file syslog yang telah disimpan di direktori home/rifqi/Documents/PraktikumSisop, kemudian melakukan enkripsi dengan cara mengganti setiap huruf dalam file syslog dengan huruf yang berada pada posisi yang sama di atasnya dalam urutan abjad sebanyak H kali. Setelah itu, hasil enkripsi akan disimpan di file yang sama dengan nama yang sama (overwrite file asli).

* Untuk menjalankan script secara otomatis, kita dapat menambahkan perintah `0 */2 * * * /bin/bash /home/rifqi/Documents/PraktikumSisop/log_decrypt.sh` ke dalam crontab. Perintah tersebut akan menjalankan script setiap 2 jam.

Hasil Program
![soal3](https://user-images.githubusercontent.com/88433109/224348346-a9750d98-98af-419c-a8b6-462d6e39a082.jpg)
