#!bin/bash

x=$(date "+%H")
# x=8  #untuk coba2

# agar waktu saat pertama kali menggunakan program ini bisa tercatat
# hal tersebut dipakai untuk menjadwalkan proses zip yang dilakukan setelah 24 jam dari saat zip sebelumnya

TEMP_FILE="x_awal.txt"

if [ ! -f $TEMP_FILE ]; then
    cd /home/naufalqii
    touch x_awal.txt
    cd Documents/prak_sisop/soal2
    touch x_awal.txt
    echo "$x" > x_awal.txt
fi


# ========================================================================================================
i=1

while [ -d kumpulan_$i ]
do
    i=$((i+1))
done

mkdir kumpulan_$i
cd kumpulan_$i

for ((j=1; j<=$x; j++))
do
    wget "https://loremflickr.com/320/240/indonesia,bali" -O perjalanan_$j.jpg
done

cd ..

j=1
while [ -f devil_$j.zip ]
do
    j=$((j+1))
done

# membuat zip
i=1
if [ $(cat x_awal.txt) -eq $x ]; then
    while [ -d kumpulan_$i ]
    do
        zip -r devil_$j.zip *kumpulan*
            i=$((i+1))
    done
    rm -r *kumpulan*
fi


# crontab -e
# 0 */10 * * * /bin/bash /home/naufalqii/Documents/prak_sisop/soal2/kobeni_liburan.sh

