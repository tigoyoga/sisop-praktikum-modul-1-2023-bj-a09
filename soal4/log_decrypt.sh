#!/bin/bash

H=$(date +'%H')
y=$(date +'%H:%M_%d:%b:%Y')

sudo cat /var/log/syslog > /home/rifqi/Documents/PraktikumSisop/$y.txt


lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)


echo -n "$(cat $y.txt | tr "${lowercase[$H]}-z}a-${lowercase[$H-1]}" 'a-z' | tr "${uppercase[$H]}-Z}A-${uppercase[$H-1]}" 'A-Z' )" > "$y.txt"


# echo -n "$(cat 21:00_03:Mar:2023.txt | tr "${lowercase[$H]}-z}a-${lowercase[$H-1]}" 'a-z' | tr "${uppercase[$H]}-Z}A-${uppercase[$H-1]}" 'A-Z' )" > "$y.txt"

# crontab
# 0 */2 * * * /bin/bash /home/rifqi/Documents/PraktikumSisop/log_decrypt.sh
