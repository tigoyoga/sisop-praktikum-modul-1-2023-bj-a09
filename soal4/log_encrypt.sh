# #!/bin/bash

H=$(date +'%H')
y=$(date +"%H:%M_%d:%b:%Y")

sudo cat /var/log/syslog > /home/rifqi/Documents/PraktikumSisop/$y.txt

# buat array untuk mendapatkan value per hurufnya
lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)


# str untuk menyimpan string hasil enkripsi
echo -n "$(cat $y.txt | tr 'a-z' "${lowercase[$H]}-z}a-${lowercase[$H-1]}" | tr 'A-Z' "${uppercase[$H]}-Z}A-${uppercase[$H-1]}")" > "$y.txt"

# 0 */2 * * * /bin/bash /home/rifqi/Documents/PraktikumSisop/log_encrypt.sh