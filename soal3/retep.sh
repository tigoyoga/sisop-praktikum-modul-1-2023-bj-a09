#!/bin/bash

timestamp="$(date '+%y/%m/%d %H:%M:%S')"

validate_log_username(){
    if ! grep -q "^u-$username" users/users.txt; then
        echo "Username tidak ada !"
        return 1;
    fi
        return 0;
}

validate_log_password(){
    if grep -q "^u-$username:p-$password$" users/users.txt; then
        echo "Login berhasil"
        echo "$timestamp LOGIN: USER $username logged in" >> log.txt
        return 0
    else
        echo "Password salah!"
        echo "$timestamp LOGIN: ERROR Failed login attempt on username $username" >> log.txt
    fi
        return 1
}

echo -e "\n------------------LOGIN------------------"

while true; do
read -p "Masukkan username: " username

if validate_log_username; then 
    break;
else
    continue;
fi
done

while true; do
read -p "Masukkan password: " password

if validate_log_password; then
    break;
else
    continue;
fi
done



