#!/bin/bash

validate_reg_username() {
    if grep -qi "u-$username" users/users.txt; then
        echo "Username sudah terdaftar, silahkan gunakan username lain !"
        echo "${timestamp} REGISTER: ERROR User already exists" >>  log.txt
    else
        return 0
    fi

    return 1
}

validate_reg_password() {
    if [[ ${#password} -lt 8 ]]; then
        echo "Password minimal 8 karakter !"
        return 1
    fi

    if ! [[ $password =~ [[:upper:]] && $password =~ [[:lower:]] ]]; then
        echo "Password harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
        return 1
    fi

    if ! [[ $password =~ ^[a-zA-Z0-9]+$ ]]; then
        echo "Password harus alphanumeric"
        return 1
    fi

    if ! [[ $password =~ [0-9] ]]; then
        echo "Password harus mengandung angka"
        return 1
    fi

    if [[ $password == $username ]]; then 
        echo "Password tidak boleh sama dengan username"
        return 1
    fi

    if echo ${password} | grep -i 'chicken\|ernie'; then 
        echo "Password tidak boleh mengandung kata chicken atau ernie"
        return 1
    fi

}

if [ -e users ]; then
    cd users
    touch users.txt
    cd ..
else
    mkdir users
    cd users
    touch users.txt
    cd ..
fi

timestamp="$(date '+%y/%m/%d %H:%M:%S')"

echo -e "\n------------------REGISTER------------------"

while true; do
read -p "Masukkan username: " username

if validate_reg_username; then
    break;
else
    continue;
fi
done

while true; do
read -p "Masukkan password: " password

if validate_reg_password; then
    break;
else
    continue;
fi
done

echo -e "\n"


echo "u-$username:p-$password" >> users/users.txt
echo "$timestamp REGISTER: INFO User $username registered successfully" >>  log.txt

echo "Registrasi berhasil !"


