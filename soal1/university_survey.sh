#!/bin/bash

# 1 a
echo "============================== 1a =============================="
awk -F',' '$4=="Japan" {print}' QSW.xls | head -5 

# 1 b
echo "============================== 1b =============================="
awk -F',' '$4=="Japan" {print $9","$2","$4","$1}' QSW.xls | head -5 | sort -n | tail -5  

# 1 c
echo "============================== 1c =============================="

awk -F',' '$4=="Japan" {print $18","$2","$4","$1}' QSW.xls | sort -n | head -10

# 1 d
echo "============================== 1d =============================="
awk '/Keren/{print}' QSW.xls


# -F',' ==> untuk memberitahu separatornya comma
# sort -n ==> sorting berdasarkan kolom yg ditunjuk pertama setelah print ($9)
